package com.example.android.proyecto2plataformas;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

import com.github.fafaldo.fabtoolbar.widget.FABToolbarLayout;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class MainENote extends AppCompatActivity {
    private FABToolbarLayout morph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_enote);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Set the editText with text from notesArray that is text from a old note
        EditText editText = (EditText) findViewById(R.id.text_editor2);
        editText.setText(MainNotePad2.notesArray.get(MainNotePad2.itemNumber));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        morph = (FABToolbarLayout) findViewById(R.id.fabtoolbar);

        View delete, save;
        delete = findViewById(R.id.delete);
        save = findViewById(R.id.save);

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                morph.hide();
                MainNotePad2.notesArray.remove(MainNotePad2.itemNumber);
                MainNotePad2.displayArray.remove(MainNotePad2.itemNumber);

                MainNotePad2.theListView.setAdapter(MainNotePad2.theAdapter);

                try {
                    FileOutputStream fileOutputStream = openFileOutput("noteSaves", Context.MODE_PRIVATE);
                    ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

                    objectOutputStream.writeObject(MainNotePad2.notesArray);
                    objectOutputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                finish();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                morph.hide();
                //Get text from user input in the EditText and add it to the listview using adapter
                EditText editText = (EditText) findViewById(R.id.text_editor2);
                String theText = (String) editText.getText().toString();

                //set updated note to notes Array
                MainNotePad2.notesArray.set(MainNotePad2.itemNumber, theText);

                //set updated note up to newline to display array
                String[] beforeNewline = theText.split("\n", 20);
                MainNotePad2.displayArray.set(MainNotePad2.itemNumber, beforeNewline[0]);

                //update the listview on main activity
                MainNotePad2.theListView.setAdapter(MainNotePad2.theAdapter);

                //write notesArray to memory
                try {
                    FileOutputStream fileOutputStream = openFileOutput("noteSaves", Context.MODE_PRIVATE);
                    ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

                    objectOutputStream.writeObject(MainNotePad2.notesArray);
                    objectOutputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                finish();
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                morph.show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu2) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu2);
        return true;
    }

}
