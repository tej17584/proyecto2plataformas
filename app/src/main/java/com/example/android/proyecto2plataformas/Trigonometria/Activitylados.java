package com.example.android.proyecto2plataformas.Trigonometria;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.proyecto2plataformas.R;

public class Activitylados extends AppCompatActivity {
    Spinner lados;
    Spinner opciones;
    Spinner angulos;
    Button operar;
    TextView respuesta;
    EditText editText1;
    EditText editText2;
    ArrayAdapter<String> adapterlados;
    ArrayAdapter<String> adapterangulos;
    ArrayAdapter<String> adapterlados2;
    String[] ladoa = {"b","c"};
    String[] ladob = {"a","c"};
    String[] ladoc = {"a","b"};
    Calcularlados operacion = new Calcularlados();
    int opcionlados=0;
    int opcionangulos=0;
    int opcionlados2=0;
    String[] lad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activitylados);
        lados = (Spinner) findViewById(R.id.lados);
        opciones = (Spinner) findViewById(R.id.opciones);
        angulos = (Spinner) findViewById(R.id.angulos);
        operar = (Button) findViewById(R.id.Operar);
        respuesta = (TextView) findViewById(R.id.Respuesta);
        editText1 = (EditText) findViewById(R.id.editText1);
        editText2 = (EditText) findViewById(R.id.editText2);

        String[] op = {"Encontrar a","Encontrar b","Encontrar c"};
        final String[] ang ={"α","β"};


        opciones.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, op));
        angulos.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ang));

        opciones.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0: //Lado a
                        adapterlados = new ArrayAdapter<String>(Activitylados.this,android.R.layout.simple_spinner_item,ladoa);
                        opcionlados = 0;
                        break;
                    case 1: //Lado b
                        adapterlados = new ArrayAdapter<String>(Activitylados.this,android.R.layout.simple_spinner_item,ladob);
                        opcionlados =1;
                        break;
                    case 2: //Lado b
                        adapterlados = new ArrayAdapter<String>(Activitylados.this,android.R.layout.simple_spinner_item,ladoc);
                        opcionlados = 2;
                        break;
                }
                lados.setAdapter(adapterlados);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        angulos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {  //Listener de angulos
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0:
                        opcionangulos = 0;
                        break;
                    case 1:
                        opcionangulos = 1;
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        lados.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0:
                        opcionlados2 = 0;
                        break;
                    case 1:
                        opcionlados2 = 1;
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        operar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((editText1.getText().toString().equals(""))||(editText2.getText().toString().equals(""))) {
                    Toast.makeText(getApplicationContext(), "Debe llenar todos los campos", Toast.LENGTH_SHORT).show();
                } else {
                    double lado = Double.parseDouble(String.valueOf(editText1.getText()));
                    double angulo = Double.parseDouble(String.valueOf(editText2.getText()));
                    if ((lado <= 0) || (angulo <= 0)) {
                        /**Editable nwtext = new SpannableStringBuilder("Debe ingresar un número mayor a 0");
                         respuesta.setText(nwtext);*/
                        Toast.makeText(getApplicationContext(), "El número ingresado debe ser mayor que 0", Toast.LENGTH_SHORT).show();
                    } else {
                        respuesta.setText(operacion.calcularLado(opcionlados, opcionlados2, opcionangulos, lado, angulo));
                    }
                }
            }
        });
    }

}
