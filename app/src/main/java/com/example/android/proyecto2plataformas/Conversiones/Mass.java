package com.example.android.proyecto2plataformas.Conversiones;

/**
 * Created by SDiego on 3/14/2018.
 */

public class Mass {
    private String kilograms;
    private String grams;
    private String miligrams;
    private String pounds;
    private String tones;
    private String ounces;
    private String stones;
    private String[] list;

    public Mass() {
        kilograms = "Kilograms(kg): ";
        grams = "Grams(g): ";
        miligrams = "Miligrams(mg): ";
        pounds = "Pounds(lb): ";
        tones = "Tones(ton): ";
        ounces = "Ounces(oz): ";
        stones = "Stones(st): ";
    }

    public String[] Kilograms(double Kilograms){
        Clear();
        double g = Kilograms*1000;
        double mg = g*1000;
        double lb = Kilograms*2.20462;
        double ton = Kilograms/1000;
        double oz = lb*16;
        double st = Kilograms/6.35029;
        grams = grams + String.format("%.6f",g);
        miligrams = miligrams + String.format("%.6f",mg);
        pounds = pounds + String.format("%.6f",lb);
        tones = tones + String.format("%.6f",ton);
        ounces = ounces+ String.format("%.6f",oz);
        stones = stones + String.format("%.6f",st);
        list = new String[]{grams,miligrams,pounds,tones,ounces,stones};
        return list;
    }

    public String[] Grams(double Grams){
        Clear();
        double kg = Grams/1000;
        double mg = Grams*1000;
        double lb = Grams/453.592;
        double ton = kg/1000;
        double oz = lb*16;
        double st = kg/6.35029;
        kilograms = kilograms + String.format("%.6f",kg);
        miligrams = miligrams + String.format("%.6f",mg);
        pounds = pounds + String.format("%.6f",lb);
        tones = tones + String.format("%.10f",ton);
        ounces = ounces+ String.format("%.6f",oz);
        stones = stones + String.format("%.10f",st);
        list = new String[]{kilograms,miligrams,pounds,tones,ounces,stones};
        return list;
    }

    public String[] Miligrams(double Miligrams){
        Clear();
        double g = Miligrams/1000;
        double kg = g/1000;
        double lb = g/453.592;
        double ton = kg/1000;
        double oz = lb*16;
        double st = kg/6.35029;
        kilograms = kilograms + String.format("%.10f",kg);
        grams = grams + String.format("%.6f",g);
        pounds = pounds + String.format("%.10f",lb);
        tones = tones + String.format("%.12f",ton);
        ounces = ounces+ String.format("%.10f",oz);
        stones = stones + String.format("%.12f",st);
        list = new String[]{kilograms,grams,pounds,tones,ounces,stones};
        return list;
    }

    public String[] Pounds(double Pounds){
        Clear();
        double g = Pounds*453.592;
        double kg = g/1000;
        double mg = g*1000;
        double ton = kg/1000;
        double oz = Pounds*16;
        double st = kg/6.35029;
        kilograms = kilograms + String.format("%.6f",kg);
        grams = grams + String.format("%.6f",g);
        miligrams = miligrams + String.format("%.6f",mg);
        tones = tones + String.format("%.8f",ton);
        ounces = ounces+ String.format("%.6f",oz);
        stones = stones + String.format("%.6f",st);
        list = new String[]{kilograms,grams,miligrams,tones,ounces,stones};
        return list;
    }

    public String[] Tones(double Tones){
        Clear();
        double kg = Tones*1000;
        double g = kg*1000;
        double mg = g*1000;
        double lb = kg*2.20462;
        double oz = lb*16;
        double st = kg/6.35029;
        kilograms = kilograms + String.format("%.6f",kg);
        grams = grams + String.format("%.6f",g);
        miligrams = miligrams + String.format("%.6f",mg);
        pounds = pounds + String.format("%.6f",lb);
        ounces = ounces+ String.format("%.6f",oz);
        stones = stones + String.format("%.6f",st);
        list = new String[]{kilograms,grams,miligrams,pounds,ounces,stones};
        return list;
    }

    public String[] Ounces(double Ounces){
        Clear();
        double kg = Ounces/35.274;
        double g = kg*1000;
        double mg = g*1000;
        double lb = kg*2.20462;
        double ton = kg/1000;
        double st = kg/6.35029;
        kilograms = kilograms + String.format("%.6f",kg);
        grams = grams + String.format("%.6f",g);
        miligrams = miligrams + String.format("%.6f",mg);
        pounds = pounds + String.format("%.6f",lb);
        tones = tones + String.format("%.10f",ton);
        stones = stones + String.format("%.6f",st);
        list = new String[]{kilograms,grams,miligrams,pounds,tones,stones};
        return list;
    }

    public String[] Stones(double Stones){
        Clear();
        double kg = Stones*6.35029;
        double g = kg*1000;
        double mg = g*1000;
        double lb = kg*2.20462;
        double ton = kg/1000;
        double oz = lb*16;
        kilograms = kilograms + String.format("%.6f",kg);
        grams = grams + String.format("%.6f",g);
        miligrams = miligrams + String.format("%.6f",mg);
        pounds = pounds + String.format("%.6f",lb);
        tones = tones + String.format("%.6f",ton);
        ounces = ounces + String.format("%.6f",oz);
        list = new String[]{kilograms,grams,miligrams,pounds,tones,ounces};
        return list;
    }

    private void Clear() {
        kilograms = "Kilograms(kg): ";
        grams = "Grams(g): ";
        miligrams = "Miligrams(mg): ";
        pounds = "Pounds(lb): ";
        tones = "Tones(ton): ";
        ounces = "Ounces(oz): ";
        stones = "Stones(st): ";
    }



}
