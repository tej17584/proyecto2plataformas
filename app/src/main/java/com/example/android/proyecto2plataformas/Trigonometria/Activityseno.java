package com.example.android.proyecto2plataformas.Trigonometria;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.proyecto2plataformas.R;

public class Activityseno extends AppCompatActivity {
    Spinner opcion;
    Spinner lados;
    EditText editText3;
    EditText editText4;
    EditText editText5;
    TextView Resultado;
    Button operar;
    ArrayAdapter<String> adapteropciones;
    ArrayAdapter<String> adapterlados;
    String[] opciones = {"Buscar lado","Buscar ángulo"};
    int nopcion=0;
    int nlados=0;
    String[] lad1 = {"Buscar lado a","Buscar lado b","Buscar lado c"};
    String[] ang1 = {"Buscar ángulo α","Buscar ángulo β","Buscar ángulo γ"};
    Calcularlados calcularlados = new Calcularlados();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activityseno);

        opcion = (Spinner) findViewById(R.id.opcion);
        lados = (Spinner) findViewById(R.id.lados);
        editText3 = (EditText) findViewById(R.id.editText3);
        editText4 = (EditText) findViewById(R.id.editText4);
        editText5 = (EditText) findViewById(R.id.editText5);
        Resultado = (TextView) findViewById(R.id.Resultado2);
        operar = (Button) findViewById(R.id.Operar2);
        opcion.setAdapter(new ArrayAdapter<String>(Activityseno.this,android.R.layout.simple_spinner_item,opciones));

        opcion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0: //En caso que quiera elegir un lado
                        adapterlados = new ArrayAdapter<String>(Activityseno.this,android.R.layout.simple_spinner_item,lad1);
                        nopcion = 0;
                        break;
                    case 1: //En caso que quiera elegir un angulo
                        adapterlados = new ArrayAdapter<String>(Activityseno.this,android.R.layout.simple_spinner_item,ang1);
                        nopcion = 1;
                        break;
                }
                lados.setAdapter(adapterlados);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        lados.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0: //En caso que escoja a, o alfa
                        nlados = 0;
                        if(nopcion==0){
                            CharSequence tex1 = "Ingrese alfa";
                            CharSequence tex2 = "Ingrese lado b/c";
                            CharSequence tex3 = "Ingrese angulo beta/gamma";
                            editText3.setHint(tex1);
                            editText4.setHint(tex2);
                            editText5.setHint(tex3);
                        }else
                        if(nopcion==1){
                            CharSequence tex1 = "Ingrese lado A";
                            CharSequence tex2 = "Ingrese lado b/c";
                            CharSequence tex3 = "Ingrese angulo beta/gamma";
                            editText3.setHint(tex1);
                            editText4.setHint(tex2);
                            editText5.setHint(tex3);
                        }
                        break;
                    case 1: //En caso que escoja b, o beta
                        nlados = 1;
                        if(nopcion==0){
                            CharSequence tex1 = "Ingrese beta";
                            CharSequence tex2 = "Ingrese lado a/c";
                            CharSequence tex3 = "Ingrese angulo alfa/gamma";
                            editText3.setHint(tex1);
                            editText4.setHint(tex2);
                            editText5.setHint(tex3);
                        }else
                        if(nopcion==1){
                            CharSequence tex1 = "Ingrese lado B";
                            CharSequence tex2 = "Ingrese lado a/c";
                            CharSequence tex3 = "Ingrese angulo alfa/gamma";
                            editText3.setHint(tex1);
                            editText4.setHint(tex2);
                            editText5.setHint(tex3);
                        }
                        break;
                    case 2: //En caso que escoja c, o gamma
                        nlados = 2;
                        if(nopcion==0){
                            CharSequence tex1 = "Ingrese gamma";
                            CharSequence tex2 = "Ingrese lado a/b";
                            CharSequence tex3 = "Ingrese angulo alfa/beta";
                            editText3.setHint(tex1);
                            editText4.setHint(tex2);
                            editText5.setHint(tex3);
                        }else
                        if(nopcion==1){
                            CharSequence tex1 = "Ingrese lado C";
                            CharSequence tex2 = "Ingrese lado a/b";
                            CharSequence tex3 = "Ingrese angulo alfa/beta";
                            editText3.setHint(tex1);
                            editText4.setHint(tex2);
                            editText5.setHint(tex3);
                        }
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        operar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if((editText3.getText().toString().equals(""))||(editText4.getText().toString().equals(""))||(editText5.getText().toString().equals(""))){
                    Toast.makeText(getApplicationContext(), "Debe llenar todos los campos", Toast.LENGTH_SHORT).show();
                }else{
                    double texto1 = Double.parseDouble(String.valueOf(editText3.getText()));
                    double texto2 = Double.parseDouble(String.valueOf(editText4.getText()));
                    double texto3 = Double.parseDouble(String.valueOf(editText5.getText()));
                    if((texto1<=0)||(texto2<=0)||(texto3<=0)){
                        Toast.makeText(getApplicationContext(), "El número ingresado debe ser mayor que 0", Toast.LENGTH_SHORT).show();
                    }else{
                        Resultado.setText(calcularlados.calcularSeno(nopcion,nlados,texto1,texto2,texto3));//String.valueOf(res));//calcularlados.calcularSeno(nopcion,nlados,texto1,texto2,texto3));
                    }
                }
            }
        });
    }
}
