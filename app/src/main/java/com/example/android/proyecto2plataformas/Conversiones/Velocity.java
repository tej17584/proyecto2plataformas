package com.example.android.proyecto2plataformas.Conversiones;

/**
 * Created by SDiego on 3/27/2018.
 */

public class Velocity {
    private String mileshour;
    private String feetsecond;
    private String meterssecond;
    private String kilometershour;
    private String knots;
    private String[] list;

    public Velocity(){
        mileshour = "Miles per hour(mi/h): ";
        feetsecond = "Feet per second(ft/s): ";
        meterssecond = "Meters per second(m/s): ";
        kilometershour = "Kilometers per hour(km/h): ";
        knots = "Knots(kn): ";
    }

    public String[] MilesHour(double MilesHour){
        Clear();
        double ftsec = MilesHour*1.46667;
        double msec = ftsec/3.28084;
        double kmhr = msec*3.6;
        double knot = kmhr/1.852;
        feetsecond = feetsecond + String.format("%.6f",ftsec);
        meterssecond = meterssecond + String.format("%.6f",msec);
        kilometershour = kilometershour + String.format("%.6f",kmhr);
        knots = knots + String.format("%.6f",knot);
        list = new String[]{feetsecond,meterssecond,kilometershour,knots};
        return list;
    }

    public String[] FeetSecond(double FeetSecond){
        Clear();
        double mihr = FeetSecond/1.46667;
        double msec = FeetSecond/3.28084;
        double kmhr = msec*3.6;
        double knot = kmhr/1.852;
        mileshour = mileshour + String.format("%.6f",mihr);
        meterssecond = meterssecond + String.format("%.6f",msec);
        kilometershour = kilometershour + String.format("%.6f",kmhr);
        knots = knots + String.format("%.6f",knot);
        list = new String[]{mileshour,meterssecond,kilometershour,knots};
        return list;
    }

    public String[] MetersSecond(double MetersSecond){
        Clear();
        double ftsec = MetersSecond*3.28084;
        double mihr = ftsec/1.46667;
        double kmhr = MetersSecond*3.6;
        double knot = kmhr/1.852;
        mileshour = mileshour + String.format("%.6f",mihr);
        feetsecond = feetsecond + String.format("%.6f",ftsec);
        kilometershour = kilometershour + String.format("%.6f",kmhr);
        knots = knots + String.format("%.6f",knot);
        list = new String[]{mileshour,feetsecond,kilometershour,knots};
        return list;
    }

    public String[] KilometersHour(double KilometersHour){
        Clear();
        double msec = KilometersHour/3.6;
        double ftsec = msec*3.28084;
        double mihr = ftsec/1.46667;
        double knot = KilometersHour/1.852;
        mileshour = mileshour + String.format("%.6f",mihr);
        feetsecond = feetsecond + String.format("%.6f",ftsec);
        meterssecond = meterssecond + String.format("%.6f",msec);
        knots = knots + String.format("%.6f",knot);
        list = new String[]{mileshour,feetsecond, meterssecond,knots};
        return list;
    }


    public String[] Knots(double Knots){
        Clear();
        double msec = Knots/1.94384;
        double ftsec = msec*3.28084;
        double mihr = ftsec/1.46667;
        double kmhr = Knots*1.852;
        mileshour = mileshour + String.format("%.6f",mihr);
        feetsecond = feetsecond + String.format("%.6f",ftsec);
        meterssecond = meterssecond + String.format("%.6f",msec);
        kilometershour = kilometershour + String.format("%.6f",kmhr);
        list = new String[]{mileshour,feetsecond,meterssecond,kilometershour};
        return list;
    }

    private void Clear(){
        mileshour = "Miles per hour(mi/h): ";
        feetsecond = "Feet per second(ft/s): ";
        meterssecond = "Meters per second(m/s): ";
        kilometershour = "Kilometers per hour(km/h): ";
        knots = "Knots(kn): ";
    }
}
