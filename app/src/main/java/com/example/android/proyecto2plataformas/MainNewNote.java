package com.example.android.proyecto2plataformas;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

import com.github.fafaldo.fabtoolbar.widget.FABToolbarLayout;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class MainNewNote extends AppCompatActivity {
    private FABToolbarLayout morph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_new_note);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        morph = (FABToolbarLayout) findViewById(R.id.fabtoolbar);

        View delete, save;
        delete = findViewById(R.id.delete);
        save = findViewById(R.id.save);

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                morph.hide();
                finish();
            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editText = (EditText) findViewById(R.id.text_editor);

                //Get text from user input in the EditText and add it to the listview using adapter
                String theText = (String) editText.getText().toString();
                MainNotePad2.notesArray.add(theText);

                String[] beforeNewline = theText.split("\n", 20);
                MainNotePad2.displayArray.add(beforeNewline[0]);

                MainNotePad2.theListView.setAdapter(MainNotePad2.theAdapter);

                try {
                    FileOutputStream fileOutputStream = openFileOutput("noteSaves", Context.MODE_PRIVATE);
                    ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

                    objectOutputStream.writeObject(MainNotePad2.notesArray);
                    objectOutputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                morph.hide();
                finish();
            }
        });


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                morph.show();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


}
