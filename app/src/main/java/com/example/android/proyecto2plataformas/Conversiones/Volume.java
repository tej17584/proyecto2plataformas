package com.example.android.proyecto2plataformas.Conversiones;

/**
 * Created by SDiego on 3/26/2018.
 */

public class Volume {
    private String liters;
    private String mililiters;
    private String cubicmeters;
    private String cubicfeet;
    private String cubicinches;
    private String USgallons;
    private String IMPgallons;
    private String[] list;

    public Volume(){
        liters ="Liters(l): ";
        mililiters="Mililiters(ml): ";
        cubicmeters="Cubic meters(m^3): ";
        cubicfeet="Cubic feet(ft^3): ";
        cubicinches="Cubic inches(in^3): ";
        USgallons="American Gallons(gal): ";
        IMPgallons="Imperial Gallons(gal): ";
    }

    public String[] Liters(double Liters){
        Clear();
        double ml =  Liters*1000;
        double m3 = Liters/1000;
        double ft3 = Liters/28.3168;
        double in3 = ft3*1728;
        double usgal = Liters/3.78541;
        double impgal = Liters/4.54609;
        mililiters = mililiters + String.format("%.6f",ml);
        cubicmeters = cubicmeters + String.format("%.6f",m3);
        cubicfeet = cubicfeet + String.format("%.6f",ft3);
        cubicinches = cubicinches + String.format("%.6f",in3);
        USgallons = USgallons + String.format("%.6f",usgal);
        IMPgallons = IMPgallons + String.format("%.6f",impgal);
        list = new String[]{mililiters,cubicmeters,cubicfeet,cubicinches,USgallons,IMPgallons};
        return list;
    }

    public String[] MiliLiters(double MiliLiters){
        Clear();
        double l =  MiliLiters/1000;
        double m3 = l/1000;
        double ft3 = l/28.3168;
        double in3 = ft3*1728;
        double usgal = l/3.78541;
        double impgal = l/4.54609;
        liters = liters + String.format("%.6f",l);
        cubicmeters = cubicmeters + String.format("%.10f",m3);
        cubicfeet = cubicfeet + String.format("%.8f",ft3);
        cubicinches = cubicinches + String.format("%.6f",in3);
        USgallons = USgallons + String.format("%.8f",usgal);
        IMPgallons = IMPgallons + String.format("%.8f",impgal);
        list = new String[]{liters,cubicmeters,cubicfeet,cubicinches,USgallons,IMPgallons};
        return list;
    }

    public String[] CubicMeters(double CubicMeters){
        Clear();
        double l =  CubicMeters*1000;
        double ml = l*1000;
        double ft3 = l/28.3168;
        double in3 = ft3*1728;
        double usgal = l/3.78541;
        double impgal = l/4.54609;
        liters = liters + String.format("%.6f",l);
        mililiters = mililiters + String.format("%.6f",ml);
        cubicfeet = cubicfeet + String.format("%.6f",ft3);
        cubicinches = cubicinches + String.format("%.6f",in3);
        USgallons = USgallons + String.format("%.6f",usgal);
        IMPgallons = IMPgallons + String.format("%.6f",impgal);
        list = new String[]{liters,mililiters,cubicfeet,cubicinches,USgallons,IMPgallons};
        return list;
    }

    public String[] CubicFeet(double CubicFeet){
        Clear();
        double l = CubicFeet*28.3168;
        double ml = l*1000;
        double m3 = l/1000;
        double in3 = CubicFeet*1728;
        double usgal = l/3.78541;
        double impgal = l/4.54609;
        liters = liters + String.format("%.6f",l);
        mililiters = mililiters + String.format("%.6f",ml);
        cubicmeters = cubicmeters + String.format("%.6f",m3);
        cubicinches = cubicinches + String.format("%.6f",in3);
        USgallons = USgallons + String.format("%.6f",usgal);
        IMPgallons = IMPgallons + String.format("%.6f",impgal);
        list = new String[]{liters,mililiters,cubicmeters,cubicinches,USgallons,IMPgallons};
        return list;
    }

    public String[] CubicInches(double CubicInches){
        Clear();
        double l = CubicInches/61.0237;
        double ml = l*1000;
        double m3 = l/1000;
        double ft3 = CubicInches/1728;
        double usgal = l/3.78541;
        double impgal = l/4.54609;
        liters = liters + String.format("%.6f",l);
        mililiters = mililiters + String.format("%.6f",ml);
        cubicmeters = cubicmeters + String.format("%.8f",m3);
        cubicfeet = cubicfeet + String.format("%.6f",ft3);
        USgallons = USgallons + String.format("%.6f",usgal);
        IMPgallons = IMPgallons + String.format("%.6f",impgal);
        list = new String[]{liters,mililiters,cubicmeters,cubicfeet,USgallons,IMPgallons};
        return list;
    }

    public String[] AmericanGalons(double AmericanGalons){
        Clear();
        double l = AmericanGalons*3.78541;
        double ml = l*1000;
        double m3 = l/1000;
        double in3 = AmericanGalons*231;
        double ft3 = AmericanGalons/7.48052;
        double impgal = l/4.54609;
        liters = liters + String.format("%.6f",l);
        mililiters = mililiters + String.format("%.6f",ml);
        cubicmeters = cubicmeters + String.format("%.6f",m3);
        cubicfeet = cubicfeet + String.format("%.6f",ft3);
        cubicinches = cubicinches + String.format("%.6f",in3);
        IMPgallons = IMPgallons + String.format("%.6f",impgal);
        list = new String[]{liters,mililiters,cubicmeters,cubicfeet,cubicinches,IMPgallons};
        return list;
    }

    public String[] ImperialGalons(double ImperialGalons){
        Clear();
        double l = ImperialGalons*4.54609;
        double ml = l*1000;
        double m3 = l/1000;
        double in3 = ImperialGalons*277.419;
        double ft3 = ImperialGalons/6.22884;
        double usgal = l/3.78541;
        liters = liters + String.format("%.6f",l);
        mililiters = mililiters + String.format("%.6f",ml);
        cubicmeters = cubicmeters + String.format("%.6f",m3);
        cubicfeet = cubicfeet + String.format("%.6f",ft3);
        cubicinches = cubicinches + String.format("%.6f",in3);
        USgallons = USgallons + String.format("%.6f",usgal);
        list = new String[]{liters,mililiters,cubicmeters,cubicfeet,cubicinches,USgallons};
        return list;
    }

    private void Clear(){
        liters ="Liters(l): ";
        mililiters="Mililiters(ml): ";
        cubicmeters="Cubic meters(m^3): ";
        cubicfeet="Cubic feet(ft^3): ";
        cubicinches="Cubic inches(in^3): ";
        USgallons="American Gallons(gal): ";
        IMPgallons="Imperial Gallons(gal): ";
    }

}
