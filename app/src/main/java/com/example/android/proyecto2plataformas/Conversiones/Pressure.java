package com.example.android.proyecto2plataformas.Conversiones;

/**
 * Created by SDiego on 3/19/2018.
 */

public class Pressure {
    String atmosphere;
    String pascal;
    String psi;
    String bar;
    String torr;
    String[] list;

    public Pressure() {
        atmosphere = "Atmosphere(atm): ";
        pascal = "Pascal(Pa): ";
        psi = "PSI(lb/in2): ";
        bar = "Bar(bar): ";
        torr = "Torr(mmHg): ";

    }

    public String[] Atmosphere(double Atmosphere){
        Clear();
        double pa = Atmosphere*101325;
        double lbin = Atmosphere*14.6959;
        double ba = Atmosphere*1.01325;
        double mmhg = Atmosphere*760;
        pascal = pascal + String.format("%.6f",pa);
        psi = psi + String.format("%.6f",lbin);
        bar = bar + String.format("%.6f",ba);
        torr = torr + String.format("%.6f",mmhg);
        list = new String[]{pascal,psi,bar,torr};
        return list;
    }

    public String[] Pascal(double Pascal){
        Clear();
        double atm = Pascal/101325;
        double lbin = Pascal/6894.76;
        double ba = Pascal*100000;
        double mmhg = Pascal/133.322;
        atmosphere = atmosphere + String.format("%.8f",atm);
        psi = psi + String.format("%.8f",lbin);
        bar = bar + String.format("%.6f",ba);
        torr = torr + String.format("%.6f",mmhg);
        list = new String[]{atmosphere,psi,bar,torr};
        return list;
    }

    public String[] PSI(double PSI){
        Clear();
        double pa = PSI*6894.76;
        double atm = PSI/14.6959;
        double ba = PSI/14.5038;
        double mmhg = atm*760;
        pascal = pascal + String.format("%.6f",pa);
        atmosphere = atmosphere + String.format("%.6f",atm);
        bar = bar + String.format("%.6f",ba);
        torr = torr + String.format("%.6f",mmhg);
        list = new String[]{pascal,atmosphere,bar,torr};
        return list;
    }

    public String[] BAR(double BAR){
        Clear();
        double pa = BAR*100000;
        double lbin = BAR*14.6959;
        double atm = pa/101325;;
        double mmhg = atm*760;
        pascal = pascal + String.format("%.6f",pa);
        psi = psi + String.format("%.6f",lbin);
        atmosphere = atmosphere + String.format("%.6f",atm);
        torr = torr + String.format("%.6f",mmhg);
        list = new String[]{pascal,psi,atmosphere,torr};
        return list;
    }

    public String[] TORR(double TORR){
        Clear();
        double pa = TORR*133.322;
        double ba = TORR/750.062;
        double lbin = TORR/51.7149;
        double atm = TORR/760;
        pascal = pascal + String.format("%.6f",pa);
        psi = psi + String.format("%.6f",lbin);
        bar = bar + String.format("%.6f",ba);
        atmosphere = atmosphere + String.format("%.6f",atm);
        list = new String[]{pascal,psi,bar,atmosphere};
        return list;
    }

    private void Clear() {
        atmosphere = "Atmosphere(atm): ";
        pascal = "Pascal(Pa): ";
        psi = "PSI(lb/in2): ";
        bar = "Bar(bar): ";
        torr = "Torr(mmHg): ";

    }

}
