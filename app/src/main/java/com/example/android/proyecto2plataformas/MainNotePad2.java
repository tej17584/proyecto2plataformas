package com.example.android.proyecto2plataformas;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class MainNotePad2 extends AppCompatActivity {

    static ArrayList<String> notesArray = new ArrayList<String>();
    static ArrayList<String> displayArray = new ArrayList<String>();
    static ListAdapter theAdapter;
    static ListView theListView;
    static int itemNumber;

    //Get everything ready
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_note_pad2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainNewNote.class);
                startActivity(intent);
            }
        });

        try {
            displayArray.clear();
            FileInputStream fileInputStream = openFileInput("noteSaves");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

            notesArray = (ArrayList<String>) objectInputStream.readObject();
            objectInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //copying notes array into displayarray but only the first line
        for (String x : notesArray) {
            String[] beforeNewline = x.split("\n", 20);


            displayArray.add(beforeNewline[0]);
        }

        theAdapter = new ArrayAdapter<String>(this, R.layout.list_green_text,
                R.id.list_content, displayArray);


        theListView = (ListView) findViewById(R.id.listView);
        theListView.setAdapter(theAdapter);

        theListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                itemNumber = position;
                Intent intent = new Intent(MainNotePad2.this, MainENote.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


}
