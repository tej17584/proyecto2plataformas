package com.example.android.proyecto2plataformas;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.android.proyecto2plataformas.Conversiones.Area;
import com.example.android.proyecto2plataformas.Conversiones.Length;
import com.example.android.proyecto2plataformas.Conversiones.Mass;
import com.example.android.proyecto2plataformas.Conversiones.Pressure;
import com.example.android.proyecto2plataformas.Conversiones.Temperature;
import com.example.android.proyecto2plataformas.Conversiones.Velocity;
import com.example.android.proyecto2plataformas.Conversiones.Volume;

public class MainConversiones extends AppCompatActivity {

    Spinner spUnits,spCategories;

    String[] strMassUnits ;  //Arrays that contains each unit option for conversions
    String[] strLengthUnits;
    String[] strTempUnits;
    String[] strPressureUnits;
    String[] strAreaUnits;
    String[] strVolumeUnits;
    String[] strVelocityUnits;

    String[] listviewList;

    ArrayAdapter<String> comboAdapter;
    ArrayAdapter<String> listAdapter;
    ListView convList;
    EditText value;

    Temperature Temp; //Objects from each category of conversion
    Length Leng;
    Mass mass;
    Pressure Press;
    Area area;
    Volume volume;
    Velocity velocity;

    public MainConversiones(){
        strMassUnits = new String[]{"","kg", "g","mg", "lb", "ton", "oz","st"};
        strLengthUnits = new String[]{"","m", "cm","mm", "km", "ft", "in","yd","mi"};
        strTempUnits = new String[]{"","C", "F", "K"};
        strPressureUnits = new String[]{"","atm","pa","psi","bar","torr"};
        strAreaUnits = new String[]{"","m^2","km^2","ft^2","in^2","yd^2","mi^2","acre","Ha"};
        strVolumeUnits = new String[]{"","l","ml","m^3","ft^3","in^3","us-gal","imp-gal"};
        strVelocityUnits = new String[]{"","mi/h","ft/s","m/s","km/h","kn"};

        Temp = new Temperature();
        Leng = new Length();
        mass = new Mass();
        Press = new Pressure();
        area = new Area();
        volume = new Volume();
        velocity = new Velocity();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_conversiones);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        spCategories = (Spinner) findViewById(R.id.spinner1);
        spUnits = (Spinner) findViewById(R.id.spinner2);
        convList = (ListView) findViewById(R.id.conversionlist);
        value = (EditText) findViewById(R.id.value) ;


        spCategories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {

                switch (i){
                    case 0: //Mass
                        convList.setAdapter(null);
                        comboAdapter = new ArrayAdapter<String>(MainConversiones.this,R.layout.spinner_item_customized, strMassUnits);
                        spUnits.setAdapter(comboAdapter);

                        spUnits.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {
                                if (value.getText().length()!=0 && value.getText().toString() != "." && i!=0 ) {
                                    listAdapter = new ArrayAdapter<String>(MainConversiones.this, android.R.layout.simple_list_item_1,  MassConv(i));
                                    convList.setAdapter(listAdapter);
                                    value.addTextChangedListener(new TextWatcher() {
                                        @Override
                                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                                        @Override
                                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
                                        @Override
                                        public void afterTextChanged(Editable editable) {
                                            if (value.getText().length()!=0 && value.getText().toString() != "." && i!=0 )  {
                                                listAdapter = new ArrayAdapter<String>(MainConversiones.this, android.R.layout.simple_list_item_1, MassConv(i));
                                                convList.setAdapter(listAdapter);
                                            }
                                        }
                                    });
                                }
                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {}
                        });
                        break;
                    case 1: //length
                        convList.setAdapter(null);
                        comboAdapter = new ArrayAdapter<String>(MainConversiones.this,R.layout.spinner_item_customized, strLengthUnits);
                        spUnits.setAdapter(comboAdapter);
                        spUnits.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {
                                if (value.getText().length()!=0 && value.getText().toString() != "." && i!=0 )  {
                                    listAdapter = new ArrayAdapter<String>(MainConversiones.this, android.R.layout.simple_list_item_1, LengthConv(i));
                                    convList.setAdapter(listAdapter);
                                    value.addTextChangedListener(new TextWatcher() {
                                        @Override
                                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                                        @Override
                                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
                                        @Override
                                        public void afterTextChanged(Editable editable) {
                                            if (value.getText().length()!=0 && value.getText().toString() != "." && i!=0 )  {
                                                listAdapter = new ArrayAdapter<String>(MainConversiones.this, android.R.layout.simple_list_item_1, LengthConv(i));
                                                convList.setAdapter(listAdapter);
                                            }
                                        }
                                    });
                                }
                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {}
                        });
                        break;

                    case 2: //temperature
                        convList.setAdapter(null);
                        comboAdapter = new ArrayAdapter<String>(MainConversiones.this,R.layout.spinner_item_customized, strTempUnits);
                        spUnits.setAdapter(comboAdapter);
                        spUnits.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {
                                if (value.getText().length()!=0 && value.getText().toString() != "." && i!=0 ) {
                                    listAdapter = new ArrayAdapter<String>(MainConversiones.this, android.R.layout.simple_list_item_1, TemperatureConv(i));
                                    convList.setAdapter(listAdapter);
                                    value.addTextChangedListener(new TextWatcher() {
                                        @Override
                                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                                        @Override
                                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
                                        @Override
                                        public void afterTextChanged(Editable editable) {
                                            if (value.getText().length()!=0 && value.getText().toString() != "." && i!=0 )  {
                                                listAdapter = new ArrayAdapter<String>(MainConversiones.this, android.R.layout.simple_list_item_1, TemperatureConv(i));
                                                convList.setAdapter(listAdapter);
                                            }
                                        }
                                    });
                                }
                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {}
                        });
                        break;
                    case 3: //Pressure
                        convList.setAdapter(null);
                        comboAdapter = new ArrayAdapter<String>(MainConversiones.this,R.layout.spinner_item_customized, strPressureUnits);
                        spUnits.setAdapter(comboAdapter);
                        spUnits.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {
                                if (value.getText().length()!=0 && value.getText().toString() != "." && i!=0 ) {
                                    listAdapter = new ArrayAdapter<String>(MainConversiones.this, android.R.layout.simple_list_item_1, PresureConv(i));
                                    convList.setAdapter(listAdapter);
                                    value.addTextChangedListener(new TextWatcher() {
                                        @Override
                                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                                        @Override
                                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
                                        @Override
                                        public void afterTextChanged(Editable editable) {
                                            if (value.getText().length()!=0 && value.getText().toString() != "." && i!=0 ) {
                                                listAdapter = new ArrayAdapter<String>(MainConversiones.this, android.R.layout.simple_list_item_1, PresureConv(i));
                                                convList.setAdapter(listAdapter);
                                            }
                                        }
                                    });
                                }
                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {}
                        });
                        break;
                    case 4: //Area
                        convList.setAdapter(null);
                        comboAdapter = new ArrayAdapter<String>(MainConversiones.this,R.layout.spinner_item_customized, strAreaUnits);
                        spUnits.setAdapter(comboAdapter);
                        spUnits.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {
                                if (value.getText().length()!=0 && value.getText().toString() != "." && i!=0 ) {
                                    listAdapter = new ArrayAdapter<String>(MainConversiones.this, android.R.layout.simple_list_item_1, AreaConv(i));
                                    convList.setAdapter(listAdapter);
                                    value.addTextChangedListener(new TextWatcher() {
                                        @Override
                                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                                        @Override
                                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
                                        @Override
                                        public void afterTextChanged(Editable editable) {
                                            if (value.getText().length()!=0 && value.getText().toString() != "." && i!=0 ) {
                                                listAdapter = new ArrayAdapter<String>(MainConversiones.this, android.R.layout.simple_list_item_1, AreaConv(i));
                                                convList.setAdapter(listAdapter);
                                            }
                                        }
                                    });
                                }
                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {}
                        });
                        break;
                    case 5: //Volume
                        convList.setAdapter(null);
                        comboAdapter = new ArrayAdapter<String>(MainConversiones.this,R.layout.spinner_item_customized, strVolumeUnits);
                        spUnits.setAdapter(comboAdapter);
                        spUnits.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {
                                if (value.getText().length()!=0 && value.getText().toString() != "." && i!=0 ) {
                                    listAdapter = new ArrayAdapter<String>(MainConversiones.this, android.R.layout.simple_list_item_1, VolumeConv(i));
                                    convList.setAdapter(listAdapter);
                                    value.addTextChangedListener(new TextWatcher() {
                                        @Override
                                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                                        @Override
                                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
                                        @Override
                                        public void afterTextChanged(Editable editable) {
                                            if (value.getText().length()!=0 && value.getText().toString() != "." && i!=0 ) {
                                                listAdapter = new ArrayAdapter<String>(MainConversiones.this, android.R.layout.simple_list_item_1, VolumeConv(i));
                                                convList.setAdapter(listAdapter);
                                            }
                                        }
                                    });
                                }
                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {}
                        });
                        break;
                    case 6: //Velocity
                        convList.setAdapter(null);
                        convList.setAdapter(null);
                        comboAdapter = new ArrayAdapter<String>(MainConversiones.this,R.layout.spinner_item_customized, strVelocityUnits);
                        spUnits.setAdapter(comboAdapter);
                        spUnits.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {
                                if (value.getText().length()!=0 && value.getText().toString() != "." && i!=0 ) {
                                    listAdapter = new ArrayAdapter<String>(MainConversiones.this, android.R.layout.simple_list_item_1, VelocityConv(i));
                                    convList.setAdapter(listAdapter);
                                    value.addTextChangedListener(new TextWatcher() {
                                        @Override
                                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                                        @Override
                                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
                                        @Override
                                        public void afterTextChanged(Editable editable) {
                                            if (value.getText().length()!=0 && value.getText().toString() != "." && i!=0 ) {
                                                listAdapter = new ArrayAdapter<String>(MainConversiones.this, android.R.layout.simple_list_item_1, VelocityConv(i));
                                                convList.setAdapter(listAdapter);
                                            }
                                        }
                                    });
                                }
                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {}
                        });
                        break;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }

    public String[] MassConv(int i){
        switch (i) {
            case 1: //kg
                listviewList = mass.Kilograms(Double.parseDouble(value.getText().toString()));
                break;
            case 2: //g
                listviewList = mass.Grams(Double.parseDouble(value.getText().toString()));
                break;
            case 3: //mg
                listviewList = mass.Miligrams(Double.parseDouble(value.getText().toString()));
                break;
            case 4: //lb
                listviewList = mass.Pounds(Double.parseDouble(value.getText().toString()));
                break;
            case 5: //ton
                listviewList = mass.Tones(Double.parseDouble(value.getText().toString()));
                break;
            case 6: //oz
                listviewList = mass.Ounces(Double.parseDouble(value.getText().toString()));
                break;
            case 7: //st
                listviewList = mass.Stones(Double.parseDouble(value.getText().toString()));
                break;
        }
        return listviewList;
    }

    public String[] LengthConv(int i){
        switch (i) {
            case 1: //m
                listviewList = Leng.Meters(Double.parseDouble(value.getText().toString()));
                break;
            case 2: //cm
                listviewList = Leng.CentiMeters(Double.parseDouble(value.getText().toString()));
                break;
            case 3: //mm
                listviewList = Leng.MiliMeters(Double.parseDouble(value.getText().toString()));
                break;
            case 4: //km
                listviewList = Leng.KiloMeters(Double.parseDouble(value.getText().toString()));
                break;
            case 5: //ft
                listviewList = Leng.Feet(Double.parseDouble(value.getText().toString()));
                break;
            case 6: //in
                listviewList = Leng.Inches(Double.parseDouble(value.getText().toString()));
                break;
            case 7: //yd
                listviewList = Leng.Yards(Double.parseDouble(value.getText().toString()));
                break;
            case 8: //mi
                listviewList = Leng.Miles(Double.parseDouble(value.getText().toString()));
                break;
        }
        return listviewList;
    }

    public String[] TemperatureConv(int i){
        switch (i){
            case 1: //C
                listviewList = Temp.Celsius(Double.parseDouble(value.getText().toString()));
                break;
            case 2: //F
                listviewList = Temp.Fahrenheit(Double.parseDouble(value.getText().toString()));
                break;
            case 3: //K
                listviewList = Temp.Kelvin(Double.parseDouble(value.getText().toString()));
                break;
        }
        return listviewList;
    }

    public String[] PresureConv(int i){
        switch (i){
            case 1: //atm
                listviewList = Press.Atmosphere(Double.parseDouble(value.getText().toString()));
                break;
            case 2: //pa
                listviewList = Press.Pascal(Double.parseDouble(value.getText().toString()));
                break;
            case 3: //psi
                listviewList = Press.PSI(Double.parseDouble(value.getText().toString()));
                break;
            case 4: //bar
                listviewList =Press.BAR(Double.parseDouble(value.getText().toString()));
                break;
            case 5: //torr
                listviewList = Press.TORR(Double.parseDouble(value.getText().toString()));
                break;
        }
        return listviewList;
    }

    public String[] AreaConv(int i){
        switch (i){
            case 1: //m^2
                listviewList = area.SquareMeters(Double.parseDouble(value.getText().toString()));
                break;
            case 2: //km^2
                listviewList = area.SquareKilometers(Double.parseDouble(value.getText().toString()));
                break;
            case 3: //ft^2
                listviewList = area.SquareFeet(Double.parseDouble(value.getText().toString()));
                break;
            case 4: //in^2
                listviewList = area.SquareInches(Double.parseDouble(value.getText().toString()));
                break;
            case 5: //yd^2
                listviewList = area.SquareYards(Double.parseDouble(value.getText().toString()));
                break;
            case 6: //mi^2
                listviewList = area.SquareMiles(Double.parseDouble(value.getText().toString()));
                break;
            case 7: //acre
                listviewList = area.Acres(Double.parseDouble(value.getText().toString()));
                break;
            case 8: //Ha
                listviewList = area.Hectares(Double.parseDouble(value.getText().toString()));
                break;
        }
        return listviewList;
    }

    public String[] VolumeConv(int i){
        switch (i){
            case 1: //l
                listviewList = volume.Liters(Double.parseDouble(value.getText().toString()));
                break;
            case 2: //ml
                listviewList = volume.MiliLiters(Double.parseDouble(value.getText().toString()));
                break;
            case 3: //m^3
                listviewList = volume.CubicMeters(Double.parseDouble(value.getText().toString()));
                break;
            case 4: //ft^3
                listviewList = volume.CubicFeet(Double.parseDouble(value.getText().toString()));
                break;
            case 5: //in^3
                listviewList = volume.CubicInches(Double.parseDouble(value.getText().toString()));
                break;
            case 6: //us-gal
                listviewList = volume.AmericanGalons(Double.parseDouble(value.getText().toString()));
                break;
            case 7: //imp-gal
                listviewList = volume.ImperialGalons(Double.parseDouble(value.getText().toString()));
                break;
        }
        return listviewList;
    }

    public String[] VelocityConv(int i){
        switch (i){
            case 1: //mi/h
                listviewList = velocity.MilesHour(Double.parseDouble(value.getText().toString()));
                break;
            case 2: //ft/s
                listviewList = velocity.FeetSecond(Double.parseDouble(value.getText().toString()));
                break;
            case 3: //m/s
                listviewList = velocity.MetersSecond(Double.parseDouble(value.getText().toString()));
                break;
            case 4: //km/h
                listviewList = velocity.KilometersHour(Double.parseDouble(value.getText().toString()));
                break;
            case 5: //knot
                listviewList = velocity.Knots(Double.parseDouble(value.getText().toString()));
                break;
        }
        return listviewList;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
}
