package com.example.android.proyecto2plataformas.Trigonometria;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.proyecto2plataformas.R;

public class Activitycoseno extends AppCompatActivity {

    Spinner opciones;
    Button operar3;
    TextView respuesta;
    EditText editText6;
    EditText editText7;
    EditText editText8;
    String[] op = {"Despejar lado a","Despejar lado b","Despejar lado c"};
    Calcularlados calcularlados = new Calcularlados();
    int opcionlado = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activitycoseno);
        opciones = (Spinner) findViewById(R.id.opciones);
        operar3 =  (Button) findViewById(R.id.operar3);
        editText6 = (EditText) findViewById(R.id.editText6);
        editText7 = (EditText) findViewById(R.id.editText7);
        editText8 = (EditText) findViewById(R.id.editText8);
        respuesta = (TextView) findViewById(R.id.textView3);

        opciones.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,op));
        opciones.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                CharSequence tex1;
                CharSequence tex2;
                CharSequence tex3;
                switch (i){
                    case 0:
                        opcionlado = 0;
                        tex1 = "Ingrese lado b";
                        tex2 = "Ingrese lado c";
                        tex3 = "Ingrese angulo alfa";
                        editText6.setHint(tex1);
                        editText7.setHint(tex2);
                        editText8.setHint(tex3);
                        break;
                    case 1:
                        opcionlado = 1;
                        tex1 = "Ingrese lado a";
                        tex2 = "Ingrese lado c";
                        tex3 = "Ingrese angulo beta";
                        editText6.setHint(tex1);
                        editText7.setHint(tex2);
                        editText8.setHint(tex3);
                        break;
                    case 2:
                        opcionlado = 2;
                        tex1 = "Ingrese lado a";
                        tex2 = "Ingrese lado b";
                        tex3 = "Ingrese angulo gamma";
                        editText6.setHint(tex1);
                        editText7.setHint(tex2);
                        editText8.setHint(tex3);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        operar3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if((editText6.getText().toString().equals(""))||(editText7.getText().toString().equals(""))||(editText8.getText().toString().equals(""))){
                    Toast.makeText(getApplicationContext(), "Debe llenar todos los campos", Toast.LENGTH_SHORT).show();
                }else {
                    double lado1 =  Double.parseDouble(String.valueOf(editText6.getText()));
                    double lado2 = Double.parseDouble(String.valueOf(editText7.getText()));
                    double angulo1 = Double.parseDouble(String.valueOf(editText8.getText()));
                    if((lado1<=0)||(lado2<=0)||(angulo1<=0)){
                        Toast.makeText(getApplicationContext(), "El número ingresado debe ser mayor que 0", Toast.LENGTH_SHORT).show();
                    }else{
                        respuesta.setText(calcularlados.calcularCoseno(opcionlado,lado1,lado2,angulo1));
                    }
                }
            }
        });
    }
}
