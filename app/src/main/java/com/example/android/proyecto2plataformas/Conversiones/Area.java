package com.example.android.proyecto2plataformas.Conversiones;

public class Area {
    private String sqmeters;
    private String sqkilometers;
    private String sqfeet;
    private String sqinches;
    private String sqyards;
    private String sqmiles;
    private String acres;
    private String hectares;
    private String[] list;

    public Area(){
        sqmeters = "Square meters(m^2): ";
        sqkilometers = "Square kilometers(km^2): ";
        sqfeet = "Square feet(ft^2): ";
        sqinches = "Square inches(in^2): ";
        sqyards = "Square yards(yd^2): ";
        sqmiles="Square miles(mi^2): ";
        acres = "Acres(acre): ";
        hectares = "Hectares(Ha): ";
    }

    public String[] SquareMeters(double SquareMeters){
        Clear();
        double km2 = SquareMeters/1000000;
        double ft2 = SquareMeters*10.7639;
        double in2 = ft2*144;
        double yd2 = ft2/9;
        double mi2 = km2/2.58999;
        double acre = ft2/43560;
        double ha = ft2/107639;
        sqkilometers = sqkilometers + String.format("%.10f",km2);
        sqfeet = sqfeet + String.format("%.6f",ft2);
        sqinches = sqinches + String.format("%.6f",in2);
        sqyards = sqyards + String.format("%.6f",yd2);
        sqmiles = sqmiles + String.format("%.10f",mi2);
        acres = acres + String.format("%.6f",acre);
        hectares = hectares + String.format("%.6f",ha);
        list = new String[]{sqkilometers,sqfeet,sqinches,sqyards,sqmiles,acres,hectares};
        return list;
    }

    public String[] SquareKilometers(double SquareKilometers){
        Clear();
        double m2 = SquareKilometers*1000000;
        double ft2 = m2*10.7639;
        double in2 = ft2*144;
        double yd2 = ft2/9;
        double mi2 = SquareKilometers/2.58999;
        double acre = ft2/43560;
        double ha = ft2/107639;
        sqmeters = sqmeters + String.format("%.6f",m2);
        sqfeet = sqfeet + String.format("%.6f",ft2);
        sqinches = sqinches + String.format("%.6f",in2);
        sqyards = sqyards + String.format("%.6f",yd2);
        sqmiles = sqmiles + String.format("%.6f",mi2);
        acres = acres + String.format("%.6f",acre);
        hectares = hectares + String.format("%.6f",ha);
        list = new String[]{sqmeters,sqfeet,sqinches,sqyards,sqmiles,acres,hectares};
        return list;
    }

    public String[] SquareFeet(double SquareFeet){
        Clear();
        double m2 = SquareFeet/10.7639;
        double km2 = m2/1000000;
        double in2 = SquareFeet*144;
        double yd2 = SquareFeet/9;
        double mi2 = km2/2.58999;
        double acre = SquareFeet/43560;
        double ha = SquareFeet/107639;
        sqmeters = sqmeters + String.format("%.6f",m2);
        sqkilometers = sqkilometers + String.format("%.12f",km2);
        sqinches = sqinches + String.format("%.6f",in2);
        sqyards = sqyards + String.format("%.6f",yd2);
        sqmiles = sqmiles + String.format("%.12f",mi2);
        acres = acres + String.format("%.10f",acre);
        hectares = hectares + String.format("%.10f",ha);
        list = new String[]{sqmeters,sqkilometers,sqinches,sqyards,sqmiles,acres,hectares};
        return list;
    }

    public String[] SquareInches(double SquareInches){
        Clear();
        double ft2 = SquareInches/144;
        double m2 = ft2/10.7639;
        double km2 = m2/1000000;
        double yd2 = ft2/9;
        double mi2 = km2/2.58999;
        double acre = ft2/43560;
        double ha = ft2/107639;
        sqmeters = sqmeters + String.format("%.8f",m2);
        sqkilometers = sqkilometers + String.format("%.12f",km2);
        sqfeet = sqfeet + String.format("%.6f",ft2);
        sqyards = sqyards + String.format("%.8f",yd2);
        sqmiles = sqmiles + String.format("%.12f",mi2);
        acres = acres + String.format("%.10f",acre);
        hectares = hectares + String.format("%.10f",ha);
        list = new String[]{sqmeters,sqkilometers,sqfeet,sqyards,sqmiles,acres,hectares};
        return list;
    }

    public String[] SquareYards(double SquareYards){
        Clear();
        double ft2 = SquareYards*9;
        double m2 = ft2/10.7639;
        double km2 = m2/1000000;
        double in2 = ft2*144;
        double mi2 = km2/2.58999;
        double acre = ft2/43560;
        double ha = ft2/107639;
        sqmeters = sqmeters + String.format("%.6f",m2);
        sqkilometers = sqkilometers + String.format("%.10f",km2);
        sqfeet = sqfeet + String.format("%.6f",ft2);
        sqinches = sqinches + String.format("%.6f",in2);
        sqmiles = sqmiles + String.format("%.10f",mi2);
        acres = acres + String.format("%.8f",acre);
        hectares = hectares + String.format("%.8f",ha);
        list = new String[]{sqmeters,sqkilometers,sqfeet,sqinches,sqmiles,acres,hectares};
        return list;
    }

    public String[] SquareMiles(double SquareMiles){
        Clear();
        double ft2 = SquareMiles*27880000;
        double m2 = ft2/10.7639;
        double km2 =SquareMiles*2.5899;
        double in2 = ft2*144;
        double yd2 = ft2/9;
        double acre = SquareMiles*640;
        double ha = SquareMiles*258.999;
        sqmeters = sqmeters + String.format("%.6f",m2);
        sqkilometers = sqkilometers + String.format("%.6f",km2);
        sqfeet = sqfeet + String.format("%.6f",ft2);
        sqinches = sqinches + String.format("%.6f",in2);
        sqyards= sqyards + String.format("%.6f",yd2);
        acres = acres + String.format("%.6f",acre);
        hectares = hectares + String.format("%.6f",ha);
        list = new String[]{sqmeters,sqkilometers,sqfeet,sqinches,sqyards,acres,hectares};
        return list;
    }

    public String[] Acres(double Acres){
        Clear();
        double mi2 = Acres/640;
        double ft2 = mi2*27880000;
        double m2 = Acres*4046.86;
        double km2 = m2/1000000;
        double in2 = ft2*144;
        double yd2 = ft2/9;
        double ha = ft2/107639;
        sqmeters = sqmeters + String.format("%.6f",m2);
        sqkilometers = sqkilometers + String.format("%.6f",km2);
        sqfeet = sqfeet + String.format("%.6f",ft2);
        sqinches = sqinches + String.format("%.6f",in2);
        sqyards= sqyards + String.format("%.6f",yd2);
        sqmiles = sqmiles + String.format("%.6f",mi2);
        hectares = hectares + String.format("%.6f",ha);
        list = new String[]{sqmeters,sqkilometers,sqfeet,sqinches,sqyards,sqmiles,hectares};
        return list;
    }

    public String[] Hectares(double Hectares){
        Clear();
        double ft2 = Hectares*107639;
        double m2 = ft2/10.7639;
        double km2 = m2/1000000;
        double in2 = ft2*144;
        double yd2 = ft2/9;
        double acre = ft2/43560;
        double mi2 = acre/640;
        sqmeters = sqmeters + String.format("%.6f",m2);
        sqkilometers = sqkilometers + String.format("%.6f",km2);
        sqfeet = sqfeet + String.format("%.6f",ft2);
        sqinches = sqinches + String.format("%.6f",in2);
        sqyards= sqyards + String.format("%.6f",yd2);
        sqmiles = sqmiles + String.format("%.6f",mi2);
        acres = acres + String.format("%.6f",acre);
        list = new String[]{sqmeters,sqkilometers,sqfeet,sqinches,sqyards,sqmiles,acres};
        return list;
    }

    private void Clear(){
        sqmeters = "Square meters(m^2): ";
        sqkilometers = "Square kilometers(km^2): ";
        sqfeet = "Square feet(ft^2): ";
        sqinches = "Square inches(in^2): ";
        sqyards = "Square yards(yd^2): ";
        sqmiles="Square miles(mi^2): ";
        acres = "Acres(acre): ";
        hectares = "Hectares(Ha): ";
    }


}
