package com.example.android.proyecto2plataformas.Conversiones;

public class Length {
    private String meters;
    private String milimeters;
    private String centimeters;
    private String kilometers;
    private String feet;
    private String inches;
    private String yards;
    private String miles;
    String[] list;

    public Length(){
        meters = "Meters(m): ";
        centimeters= "Centimeters(cm): ";
        milimeters= "Milimeters(mm): ";
        kilometers= "Kilometers(km): ";
        feet= "Feet(ft): ";
        inches= "Inches(in): ";
        yards= "Yards(yd): ";
        miles= "Miles(mi): ";
    }

    public String[] Meters(double Meters){
        Clear();
        double cm = Meters*100;
        double mm = cm*10;
        double km = Meters/1000;
        double ft = 3.2808*Meters;
        double in = 39.3701*Meters;
        double yd = Meters/0.9144;
        double mi = Meters/1609.34;
        centimeters = centimeters + String.format("%.6f",cm);
        milimeters = milimeters + String.format("%.6f",mm);
        kilometers = kilometers + String.format("%.6f",km);
        feet = feet + String.format("%.6f",ft);
        inches = inches + String.format("%.6f",in);
        yards = yards + String.format("%.6f",yd);
        miles = miles + String.format("%.8f",mi);
        list = new String[]{centimeters,milimeters,kilometers,feet,inches,yards,miles};
        return list;
    }

    public String[] MiliMeters(double MiliMeters){
        Clear();
        double m = MiliMeters/1000;
        double cm = m*100;
        double km = m/1000;
        double ft = 0.032808*cm;
        double in = 0.393701*cm;
        double yd = cm/91.44;
        double mi = km/1.60934;
        meters = meters + String.format("%.6f",m);
        centimeters = centimeters + String.format("%.6f",cm);
        kilometers = kilometers + String.format("%.10f",km);
        feet = feet + String.format("%.6f",ft);
        inches = inches + String.format("%.6f",in);
        yards = yards + String.format("%.6f",yd);
        miles = miles + String.format("%.10f",mi);
        list = new String[]{meters,centimeters,kilometers,feet,inches,yards,miles};
        return list;
    }

    public String[] CentiMeters(double CentiMeters){
        Clear();
        double m = CentiMeters/100;
        double mm = m*1000;
        double km = m/1000;
        double ft = 0.032808*CentiMeters;
        double in = 0.393701*CentiMeters;
        double yd = CentiMeters/91.44;
        double mi = CentiMeters/160934;
        meters = meters + String.format("%.6f",m);
        milimeters = milimeters + String.format("%.6f",mm);
        kilometers = kilometers + String.format("%.10f",km);
        feet = feet + String.format("%.6f",ft);
        inches = inches + String.format("%.6f",in);
        yards = yards + String.format("%.6f",yd);
        miles = miles + String.format("%.10f",mi);
        list = new String[]{meters,milimeters,kilometers,feet,inches,yards,miles};
        return list;
    }

    public String[] KiloMeters(double KiloMeters){
        Clear();
        double m = KiloMeters*1000;
        double cm = m*100;
        double mm = m*1000;
        double ft = 3280.84*KiloMeters;
        double in = 39370.1*KiloMeters;
        double yd = KiloMeters/0.0009144;
        double mi = KiloMeters/1.60934;
        meters = meters + String.format("%.6f",m);
        centimeters = centimeters + String.format("%.6f",cm);
        milimeters = milimeters + String.format("%.6f",mm);
        feet = feet + String.format("%.6f",ft);
        inches = inches + String.format("%.6f",in);
        yards = yards + String.format("%.6f",yd);
        miles = miles + String.format("%.6f",mi);
        list = new String[]{meters,centimeters,milimeters,feet,inches,yards,miles};
        return list;
    }

    public String[] Feet(double Feet){
        Clear();
        double m = Feet/3.28084;
        double cm = m*100;
        double mm = m*1000;
        double km = m/1000;
        double in = Feet*12;
        double yd = Feet/3;
        double mi = Feet/5280;
        meters = meters + String.format("%.6f",m);
        centimeters = centimeters + String.format("%.6f",cm);
        milimeters = milimeters + String.format("%.6f",mm);
        kilometers = kilometers + String.format("%.8f",km);
        inches = inches + String.format("%.6f",in);
        yards = yards + String.format("%.6f",yd);
        miles = miles + String.format("%.8f",mi);
        list = new String[]{meters,centimeters,milimeters,kilometers,inches,yards,miles};
        return list;
    }

    public String[] Inches(double Inches){
        Clear();
        double cm = Inches*2.54;
        double m = cm/100;
        double mm = m*1000;
        double km = m/1000;
        double ft = Inches/12;
        double yd = Inches/36;
        double mi = Inches/63360;
        meters = meters + String.format("%.6f",m);
        centimeters = centimeters + String.format("%.6f",cm);
        milimeters = milimeters + String.format("%.6f",mm);
        kilometers = kilometers + String.format("%.10f",km);
        feet = feet + String.format("%.6f",ft);
        yards = yards + String.format("%.6f",yd);
        miles = miles + String.format("%.10f",mi);
        list = new String[]{meters,centimeters,milimeters,kilometers,feet,yards,miles};
        return list;
    }

    public String[] Yards(double Yards){
        Clear();
        double cm = Yards*91.44;
        double m = cm/100;
        double mm = m*1000;
        double km = m/1000;
        double ft = Yards*3;
        double in = Yards*36;
        double mi = Yards/1760;
        meters = meters + String.format("%.6f",m);
        centimeters = centimeters + String.format("%.6f",cm);
        milimeters = milimeters + String.format("%.6f",mm);
        kilometers = kilometers + String.format("%.8f",km);
        feet = feet + String.format("%.6f",ft);
        inches = inches + String.format("%.6f",in);
        miles = miles + String.format("%.8f",mi);
        list = new String[]{meters,centimeters,milimeters,kilometers,feet,inches,miles};
        return list;
    }

    public String[] Miles(double Miles){
        Clear();
        double km = Miles*1.60934;
        double m = km*1000;
        double mm = m*1000;
        double cm = m*100;
        double ft = Miles*5280;
        double in = ft*12;
        double yd = in/36;
        meters = meters + String.format("%.6f",m);
        centimeters = centimeters + String.format("%.6f",cm);
        milimeters = milimeters + String.format("%.6f",mm);
        kilometers = kilometers + String.format("%.6f",km);
        feet = feet + String.format("%.6f",ft);
        inches = inches + String.format("%.6f",in);
        yards = yards + String.format("%.6f",yd);
        list = new String[]{meters,centimeters,milimeters,kilometers,feet,inches,yards};
        return list;
    }

    private void Clear(){
        meters = "Meters(m): ";
        centimeters= "Centimeters(cm): ";
        milimeters= "Milimeters(mm): ";
        kilometers= "Kilometers(km): ";
        feet= "Feet(ft): ";
        inches= "Inches(in): ";
        yards= "Yards(yd): ";
        miles= "Miles(mi): ";
    }

}
