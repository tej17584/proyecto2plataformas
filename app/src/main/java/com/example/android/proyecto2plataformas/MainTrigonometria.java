package com.example.android.proyecto2plataformas;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.GridLayout;

import com.example.android.proyecto2plataformas.Trigonometria.Activitycoseno;
import com.example.android.proyecto2plataformas.Trigonometria.Activitylados;
import com.example.android.proyecto2plataformas.Trigonometria.Activitysemejantes;
import com.example.android.proyecto2plataformas.Trigonometria.Activityseno;

public class MainTrigonometria extends AppCompatActivity {

    GridLayout mainGrid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_trigonometria);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mainGrid = (GridLayout)findViewById(R.id.mainGrid);

        //Set Event
        setSingleEvent(mainGrid);
    /**
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }

    private void setSingleEvent(GridLayout mainGrid) {

        for (int i=0;i<mainGrid.getChildCount();i++){
            CardView cardView = (CardView)mainGrid.getChildAt(i);
            final int finalI = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Toast.makeText(MainActivity.this,"Clicked at index "+ finalI,Toast.LENGTH_SHORT).show();
                    if(finalI == 0){  //Abrir Activity de despejar lados
                        Intent intent = new Intent(MainTrigonometria.this,Activitylados.class);
                        startActivity(intent);
                    }else if(finalI == 1){  //Abrir Activity ley de senos
                        Intent intent = new Intent(MainTrigonometria.this,Activityseno.class);
                        startActivity(intent);
                    }else if(finalI == 2){  //Abrir Activity ley de cosenos
                        Intent intent = new Intent(MainTrigonometria.this,Activitysemejantes.class);
                        startActivity(intent);
                    }else if(finalI == 3){  //Abrir Activity de triangulos semejantes
                        Intent intent = new Intent(MainTrigonometria.this,Activitycoseno.class);
                        startActivity(intent);
                    }
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
}
