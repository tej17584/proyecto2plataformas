package com.example.android.proyecto2plataformas.Conversiones;

/**
 * Created by SDiego on 3/14/2018.
 */

public class Temperature {
    private String celsius;
    private String fahrenheit;
    private String kelvin;
    private String[] list;

    public Temperature(){
        celsius= "Celsius(C): ";
        fahrenheit= "Fahrenheit(F): ";
        kelvin= "Kelvin(K): ";
    }

    public String[] Celsius(double Celsius){
        Clear();
        double far = ((9.0/5.0)*Celsius)+32.0;
        double kelv = Celsius + 273.15;
        fahrenheit = fahrenheit + String.format("%.6f",far);
        kelvin = kelvin + String.format("%.6f",kelv);
        list = new String[]{fahrenheit,kelvin};
        return list;
    }

    public String[] Kelvin(double Kelvin){
        Clear();
        double cel = Kelvin - 273.15;
        double far = ((9.0/5.0)*cel) + 32.0;
        celsius = celsius + String.format("%.6f",cel);
        fahrenheit = fahrenheit + String.format("%.6f",far);
        list = new String[]{fahrenheit,celsius};
        return list;
    }

    public String[] Fahrenheit(double Fahrenheit){
        Clear();
        double cel = (Fahrenheit-32.0)*(5.0/9.0);
        double kelv = cel + 273.15;
        celsius = celsius + String.format("%.6f",cel);
        kelvin = kelvin + String.format("%.6f",kelv);
        list = new String[]{celsius,kelvin};
        return list;
    }

    private void Clear(){
        celsius= "Celsius(C): ";
        fahrenheit= "Fahrenheit(F): ";
        kelvin= "Kelvin(K): ";
    }

}
