package com.example.android.proyecto2plataformas;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Button;

import java.util.regex.Pattern;

import com.example.android.proyecto2plataformas.R;


//Cuando comence con esta calculadora
//Solo DIOS y yo sabiamos como iba a funcionar
//Ahora solo DIOS sabe y si es que todo concuerda
//

//Asi mismo, cuando comenzamos el proyecto, solo DIOS sabia como iba a hacerse
//Sigue siendo igual, solo Dios sabe como quedo todo junto
public class MainCalculadora extends AppCompatActivity {
    private TextView _screen;
    private String display = "";
    private String currentOperator= "";
    private String result = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_calculadora);
        _screen = (TextView)findViewById(R.id.textView);
        _screen.setText(display);

    }

    private void updateScreen(){

        _screen.setText(display);
    }

    public void onClickNumber(View v){
        if(result != ""){
            clear();
            updateScreen();
        }
        Button b = (Button) v;
        display += b.getText();
        updateScreen();
    }
    private boolean isOperator(char op){
        switch (op){
            case '+':
            case '-':
            case '*':
            case '/': return true;
            default: return false;
        }
    }

    public void onClickOperator(View v){
        if(display=="")return;

        Button b = (Button) v;

        if(result != ""){
            String _display = result;
            clear();
            display = _display;
        }
        if(currentOperator !="") {
            Log.d("CalcX", "" + display.charAt(display.length() - 1));
            if (isOperator(display.charAt(display.length() - 1))) {
                display = display.replace(display.charAt(display.length() - 1), b.getText().charAt(0));
                updateScreen();
                return;
            }else{
                getResult();
                display = result;
                result = "";
            }
            currentOperator = b.getText().toString();
        }
        display += b.getText();
        currentOperator = b.getText().toString();
        updateScreen();
    }

    private void clear(){
        display = "";
        currentOperator = "";
        result = "";
    }

    public void onClickClear(View v){
        clear();
        updateScreen();
    }
    private double operate(String a, String b, String op){
        switch(op){
            case "+": return Double.valueOf(a)+ Double.valueOf(b);
            case "-": return Double.valueOf(a)- Double.valueOf(b);
            case "*": return Double.valueOf(a)* Double.valueOf(b);
            case "/": try{
                return Double.valueOf(a)/ Double.valueOf(b);
            }
            catch (Exception e){
                Log.d("Calc", e.getMessage());
            }
            default: return -1;
        }
    }
    private boolean getResult(){
        if (currentOperator=="")return false;
        String [] operation = display.split(Pattern.quote(currentOperator));
        if (operation.length <2) return false;
        result = String.valueOf(operate(operation[0],operation[1],currentOperator));
        return true;
    }

    public void onClickEqual(View v){
        if (display=="") return;
        if (!getResult()) return;
        _screen.setText(display + "\n" +String.valueOf(result));
    }
}
