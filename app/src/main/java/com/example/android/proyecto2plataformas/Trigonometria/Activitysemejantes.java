package com.example.android.proyecto2plataformas.Trigonometria;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.proyecto2plataformas.R;

public class Activitysemejantes extends AppCompatActivity {

    Spinner opcionesc;
    Button operar4;
    TextView respuesta;
    EditText editext9; EditText editext10;
    EditText editext11; EditText editext12;
    EditText editext13; EditText editext14;
    String[] op = {"Criterio Lado Lado Lado","Criterio Lado Lado Ángulo","Criterio Ángulo Ángulo"};
    Calcularlados calcularlados = new Calcularlados();
    int opcioncriterio=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activitysemejantes);
        operar4 = (Button) findViewById(R.id.Operar4);
        respuesta = (TextView) findViewById(R.id.textView4);
        opcionesc = (Spinner) findViewById(R.id.spinner4);
        editext9 = (EditText) findViewById(R.id.editText9); editext10 = (EditText) findViewById(R.id.editText10);
        editext11 = (EditText) findViewById(R.id.editText11); editext12 = (EditText) findViewById(R.id.editText12);
        editext13 = (EditText) findViewById(R.id.editText13); editext14 = (EditText) findViewById(R.id.editText14);
        opcionesc.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,op));
        opcionesc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                CharSequence tex1; CharSequence tex2;
                CharSequence tex3; CharSequence tex4;
                CharSequence tex5; CharSequence tex6;
                switch (i){
                    case 0: //En caso que elija criterio lado lado lado
                        opcioncriterio = 0;
                        tex1 = "Lado a"; tex2 = "Lado b";
                        tex3 = "Lado c"; tex4 = "Lado a'";
                        tex5 = "Lado b'"; tex6 = "Lado c'";
                        editext9.setHint(tex1); editext10.setHint(tex2);
                        editext11.setHint(tex3); editext12.setHint(tex4);
                        editext13.setHint(tex5); editext14.setHint(tex6);
                        editext9.setVisibility(View.VISIBLE);editext10.setVisibility(View.VISIBLE);
                        editext11.setVisibility(View.VISIBLE);editext12.setVisibility(View.VISIBLE);
                        editext13.setVisibility(View.VISIBLE); editext14.setVisibility(View.VISIBLE);
                        break;
                    case 1: //En caso que elija criterio lado lado angulo aun falta corregir esta parte
                        opcioncriterio = 1;
                        tex1 = "Lado 1"; tex2 = "Lado 2";
                        tex3 = "Ángulo entre 1 y 2"; tex4 = "Lado 1'";
                        tex5 = "Lado 2'"; tex6 = "Ángulo entre 1' y 2'";
                        editext9.setHint(tex1); editext10.setHint(tex2);
                        editext11.setHint(tex3); editext12.setHint(tex4);
                        editext13.setHint(tex5); editext14.setHint(tex6);
                        editext9.setVisibility(View.VISIBLE);editext10.setVisibility(View.VISIBLE);
                        editext11.setVisibility(View.VISIBLE);editext12.setVisibility(View.VISIBLE);
                        editext13.setVisibility(View.VISIBLE); editext14.setVisibility(View.VISIBLE);
                        break;
                    case 2: //En caso que elija criterio angulo angulo
                        opcioncriterio = 2;
                        tex1 = "Ángulo 1"; tex2 = "Ángulo 2";
                        tex3 = "Ángulo 1'"; tex4 = "Ángulo 2'";
                        editext9.setHint(tex1); editext10.setHint(tex2);
                        editext12.setHint(tex3); editext13.setHint(tex4);
                        editext9.setVisibility(View.VISIBLE);editext10.setVisibility(View.VISIBLE);
                        editext11.setVisibility(View.INVISIBLE);editext12.setVisibility(View.VISIBLE);
                        editext13.setVisibility(View.VISIBLE); editext14.setVisibility(View.INVISIBLE);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {


            }
        });

        operar4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((opcioncriterio == 0) || (opcioncriterio == 1)) { //En caso que elija opcion 1 o 2
                    if ((editext9.getText().toString().equals("")) || (editext10.getText().toString().equals("")) || (editext11.getText().toString().equals(""))
                            || (editext12.getText().toString().equals("")) || (editext13.getText().toString().equals("")) || (editext14.getText().toString().equals(""))) {
                        Toast.makeText(getApplicationContext(), "Debe llenar todos los campos", Toast.LENGTH_SHORT).show();
                    }else{
                        double lado1 = Double.parseDouble(String.valueOf(editext9.getText()));
                        double lado2 = Double.parseDouble(String.valueOf(editext10.getText()));
                        double lado3 = Double.parseDouble(String.valueOf(editext11.getText()));
                        double lado1x = Double.parseDouble(String.valueOf(editext12.getText()));
                        double lado2x = Double.parseDouble(String.valueOf(editext13.getText()));
                        double lado3x = Double.parseDouble(String.valueOf(editext14.getText()));
                        if ((lado1 <= 0) || (lado2 <= 0) || (lado3 <= 0) || (lado1x <= 0) || (lado2x <= 0) || (lado3x <= 0)) {
                            Toast.makeText(getApplicationContext(), "El número ingresado debe ser mayor que 0", Toast.LENGTH_SHORT).show();
                        }else{
                            respuesta.setText(calcularlados.triangulosSemejantes(opcioncriterio,lado1,lado2,lado3,lado1x,lado2x,lado3x));
                            String x = calcularlados.triangulosSemejantes(opcioncriterio, lado1, lado2, lado3, lado1x, lado2x, lado3x);
                            if(x.equals("Los triángulos son semejantes")){
                                respuesta.setTextColor(Color.GREEN);
                            }else{
                                respuesta.setTextColor(Color.RED);
                            }
                        }
                    }
                }else//En caso que sea 3 la opcion
                {
                    if ((editext9.getText().toString().equals("")) || (editext10.getText().toString().equals("")) ||
                            (editext12.getText().toString().equals("")) || (editext13.getText().toString().equals(""))) {
                        Toast.makeText(getApplicationContext(), "Debe llenar todos los campos", Toast.LENGTH_SHORT).show();
                    }else{
                        double lado1 = Double.parseDouble(String.valueOf(editext9.getText()));
                        double lado2 = Double.parseDouble(String.valueOf(editext10.getText()));
                        double lado3 = 0;
                        double lado1x = Double.parseDouble(String.valueOf(editext12.getText()));
                        double lado2x = Double.parseDouble(String.valueOf(editext13.getText()));
                        double lado3x = 0;
                        if ((lado1 <= 0) || (lado2 <= 0) || (lado1x <= 0) || (lado2x <= 0)) {
                            Toast.makeText(getApplicationContext(), "El número ingresado debe ser mayor que 0", Toast.LENGTH_SHORT).show();
                        }else{
                            respuesta.setText(calcularlados.triangulosSemejantes(opcioncriterio, lado1, lado2, lado3, lado1x, lado2x, lado3x));
                            String x = calcularlados.triangulosSemejantes(opcioncriterio, lado1, lado2, lado3, lado1x, lado2x, lado3x);
                            if(x.equals("Los triángulos son semejantes")){
                                respuesta.setTextColor(Color.GREEN);
                            }else{
                                respuesta.setTextColor(Color.RED);
                            }
                        }
                    }
                }
            }
        });
    }
}
