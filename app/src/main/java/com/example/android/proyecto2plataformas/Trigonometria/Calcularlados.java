package com.example.android.proyecto2plataformas.Trigonometria;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import static java.lang.Math.toRadians;

/**
 * Created by alber on 3/28/2018.
 */

public class Calcularlados {
    public Calcularlados(){
    }

    public String calcularLado(int opcion,int oplado, int opangulo, double lado, double angulo){
        String ladoescogido="";
        double resultado =0;
        String respuesta="";
        switch (opcion){
            case 0: //En caso que quiera buscar el lado a
                ladoescogido = "El lado a es: ";
                switch (oplado){
                    case 0: //En caso que escoja b
                        switch (opangulo){
                            case 0: //En caso que escoja alfa
                                resultado = lado/Math.cos(toRadians(angulo));
                                break;
                            case 1: //En caso que escoja beta
                                resultado = lado/Math.sin(toRadians(angulo));
                                break;
                        }
                        break; //Fin de tener lado b
                    case 1: //En caso que escoja c
                        switch (opangulo){
                            case 0:  //En caso que escoja alfa
                                resultado = lado/Math.sin(toRadians(angulo));
                                break;
                            case 1: //En caso que escoja beta
                                resultado = lado/Math.cos(toRadians(angulo));
                                break;
                        }
                        break; //Fin de tener lado c
                }
                break; //fin del lado a
            case 1: //En caso que quiera buscar el lado b
                ladoescogido = "El lado b es: ";
                switch (oplado){
                    case 0: //En caso que escoja a
                        switch (opangulo){
                            case 0: //En caso que escoja alfa
                                resultado = lado*Math.cos(toRadians(angulo));
                                break;
                            case 1: //En caso que escoja beta
                                resultado = lado*Math.sin(toRadians(angulo));
                                break;
                        }
                        break; //Fin de tener lado b
                    case 1: //En caso que escoja c
                        switch (opangulo){
                            case 0:  //En caso que escoja alfa
                                resultado = lado/Math.tan(toRadians(angulo));
                                break;
                            case 1: //En caso que escoja beta
                                resultado = lado*Math.tan(toRadians(angulo));
                                break;
                        }
                        break; //Fin de tener lado c
                }
                break; //fin del lado b
            case 2: //En caso que quiera buscar el lado c
                ladoescogido = "El lado c es: ";
                switch (oplado){
                    case 0: //En caso que escoja a
                        switch (opangulo){
                            case 0: //En caso que escoja alfa
                                resultado = lado*Math.sin(toRadians(angulo));
                                break;
                            case 1: //En caso que escoja beta
                                resultado = lado*Math.cos(toRadians(angulo));
                                break;
                        }
                        break; //Fin de tener lado b
                    case 1: //En caso que escoja c
                        switch (opangulo){
                            case 0:  //En caso que escoja alfa
                                resultado = lado*Math.tan(toRadians(angulo));
                                break;
                            case 1: //En caso que escoja beta
                                resultado = lado/Math.tan(toRadians(angulo));
                                break;
                        }
                        break; //Fin de tener lado c
                }
                break; //fin del lado b

        }
        //respuesta = Double.toString(resultado);
        respuesta = ladoescogido+String.format("%.3f",resultado);
        return respuesta;
    }

    public String calcularSeno(int opcion,int opcionlado,double alfa,double beta, double gamma){
        String opcionescogida="";
        double resultado2=0;
        String respuesta2;
        switch (opcion){
            case 0: //Caso que quiera despejar un lado
                switch (opcionlado){
                    case 0: //En caso que quiera buscar el lado a
                        resultado2 = (beta/Math.sin(toRadians(gamma)))*Math.sin(toRadians(alfa));
                        opcionescogida = "El lado a es: ";
                        break;
                    case 1: //En caso que quiera buscar el lado b
                        resultado2 = (beta/Math.sin(toRadians(gamma)))*Math.sin(toRadians(alfa));
                        opcionescogida = "El lado b es: ";
                        break;
                    case 2: //En caso que quiera buscar el lado c
                        opcionescogida = "El lado c es: ";
                        break;
                }
                break; //Fin de buscar lado
            case 1: //En caso que quiera despejar un angulo
                switch (opcionlado){
                    case 0: //En caso que quiera despejar angulo alfa
                        resultado2 = Math.asin(alfa*(Math.sin(toRadians(gamma))/beta));
                        opcionescogida = "El angulo α es: ";
                        break;
                    case 1: //En caso que quiera despejar angulo beta
                        resultado2 = Math.asin(alfa*(Math.sin(toRadians(gamma))/beta));
                        opcionescogida = "El angulo β es: ";
                        break;
                    case 2: //En caso que quiera despejar angulo alfa
                        resultado2 = Math.asin(alfa*(Math.sin(toRadians(gamma))/beta));
                        opcionescogida = "El angulo γ es: ";
                        break;
                }
                break;//Fin de buscar angulo
        }
        DecimalFormatSymbols separador = new DecimalFormatSymbols();
        separador.setDecimalSeparator('.');
        DecimalFormat formato1 = new DecimalFormat("#.00",separador);
        respuesta2 = opcionescogida+formato1.format(resultado2);//String.valueOf(resultado2);
        return respuesta2;
    }

    public String calcularCoseno(int opcion,double a,double b,double c){
        String opcionescogida="";
        double resultado3=0;
        String respuesta3;
        switch (opcion){
            case 0: //Despejar el lado a
                resultado3 = Math.sqrt((a*a)+(b*b)-(2*a*b*Math.cos(toRadians(c))));
                opcionescogida = "El lado a es: ";
                break;
            case 1: //Despejar el lado b
                resultado3 = Math.sqrt((a*a)+(b*b)-(2*a*b*Math.cos(toRadians(c))));
                opcionescogida = "El lado b es: ";
                break;
            case 2: //Despejar el lado c
                resultado3 = Math.sqrt((a*a)+(b*b)-(2*a*b*Math.cos(toRadians(c))));
                opcionescogida = "El lado c es: ";
                break;
        }//Fin del switch
        DecimalFormatSymbols separador = new DecimalFormatSymbols();
        separador.setDecimalSeparator('.');
        DecimalFormat formato1 = new DecimalFormat("#.00",separador);
        respuesta3= opcionescogida+formato1.format(resultado3);//String.valueOf(resultado2);
        return respuesta3;
    }
    public String triangulosSemejantes(int opcion,double a,double b,double c,double ax,double bx,double cx){  //En caso que fuera lado lado lado, o lado lado angulo
        String respuesta = "";
        double comp1 = 0; double comp2=0; double comp3=0;
        switch (opcion){
            case 0: //Criterio lado lado lado
                comp1 = a/ax;
                comp2 = b/bx;
                comp3 = c/cx;
                if((comp1==comp2)&&(comp1==comp3)){
                    respuesta = "Los triángulos son semejantes";
                }else{
                    respuesta = "Los triángulos no son semejantes";
                }
                break;
            case 1: //Criterio lado lado angulo
                comp1 = a/ax;
                comp2 = b/bx;
                if((comp1==comp2)&&(c==cx)){
                    respuesta = "Los triángulos son semejantes";
                }else{
                    respuesta = "Los triángulos no son semejantes";
                }
                break;
            case 2: //Criterio angulo angulo
                if((a==ax)&&(b==bx)){
                    respuesta = "Los triángulos son semejantes";
                }else
                    respuesta = "Los triángulos no son semejantes";
                break;
        }
        return respuesta;
    }
}
